const buttonResultPlus = document.getElementById('result_plus');
const buttonResultMinus = document.getElementById('result_minus');
const count = document.getElementById('count');

//Прибавление единицы
function increment(actualCount) {
	if (actualCount >= 0 && actualCount < 10) {
		actualCount++;
	}
	else {
        actualCount = 0;
    }
    
    return actualCount;
} 

//Вычитание единицы
function decrement(actualCount) {
	if (actualCount > 0 && actualCount <=10) {
		actualCount--;
	}
	else {
        actualCount = 10;
    }
	return actualCount;
} 

//Прибавление и вычитание через клавиатуру
function keyPress(e, actualCount) {
    let newCount = actualCount;
    switch (e.keyCode) {
        
        case 39:
            newCount = increment(actualCount);
            break;
        case 37: 
            newCount = decrement(actualCount);
            break;            
    }
    
    return newCount;
}

//Вычитание по клику
buttonResultMinus.addEventListener('click', function() {
    count.value = decrement(+count.value);
} );

//Прибавление по клику
buttonResultPlus.addEventListener('click', function() {
    count.value = increment(+count.value);
} ); 

//Прибавление и вычитание при нажатии на клавиатуру
window.addEventListener('keydown', function (e) {
    count.value = keyPress(e, +count.value);
} );
